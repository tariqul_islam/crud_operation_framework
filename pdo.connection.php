<?php

/* To Do functions are--->  
 * 1)QueryResultDDL  ....done...
 * 2)one_many_relation  
 * 3)one_many_relation_fatchAssoc
 * 4)ExistsWithCondition  ...done....
 */

class connection {

    public function __construct() {
        
    }

    public function open() {
        $db = new PDO("pgsql:host=localhost;port=5432;dbname=price_matrix;user=postgres;password=asma123");
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //$db->setAttribute(PDO::Pgsql, $db)
        //$db->exec('SET NAMES utf8');
        return $db;
    }

    public function close() {
        //$this->open() = NULL;
    }

    public function login_activity($user_id = '') {
        $db = $this->open();
        $time = date('Y-m-d H:i:s');
        $session_data = $_SESSION['now_session'];
        $query = $db->prepare("INSERT INTO activity_log (activity_login_time,emp_id,session_data) VALUES ('$time','$user_id','$session_data')");

        $query->execute();
//        $result = $query->fetch(PDO::FETCH_OBJ);
        if ($query) {
            return 1;
        } else {
            return 0;
        }
    }

    public function logout() {
        $db = $this->open();
        $session_now = '';
        $user_id = '';
        $time = '';
//      $pagess = implode(",",$pages["page"]);
        $today = date('Y-m-d H:i:s');
        $sys_date = date_create($today);
        $time = date_format($sys_date, 'Y-m-d H:i:s');
        $user_id = $_SESSION['Employee_id'];
        $session_now = $_SESSION['now_session'];
        // Close session now
        session_write_close();

        try {
            $sql = $db->prepare("SELECT MAX(activity_log_id) AS activity_log_id FROM activity_log WHERE emp_id='$user_id' AND session_data='$session_now'");
            $sql->execute();
            $rows = $sql->fetch(PDO::FETCH_OBJ);
            $log_id = $rows->activity_log_id;

            $sql2 = $db->prepare("UPDATE activity_log SET activity_logout_time='$time' WHERE activity_log_id='$log_id'");
            $sql2->execute();
            if ($sql2) {
                unset($_SESSION["email"]);    //  unset($_SESSION['now_session']);
                session_unset();
                //session_destroy();
                return 1;
            } else {
                return 0;
            }
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    public function CheckUniqueField($object, $object_array) {
        $count = 0;

        if (count($object_array) == 1) {
            $con_key_from_arr = array_keys($object_array);
            $key = $con_key_from_arr[0];
            $value = array_shift($object_array);
            $querystring = "SELECT * FROM " . $object . " WHERE " . $key . "='" . $value . "'";
            try {
                $db = $this->open();
                $sql = $db->prepare($querystring);
                $sql->execute();
                if ($sql->rowCount() >= 1) {
                    return 1;
                } else {
                    return 0;
                }
            } catch (PDOException $ex) {
                return $ex->getMessage();
            }
        } else {
            return "Object Array is not in correct format";
        }
    }

    public function ExistsWithCondition($object, $condition, $field_name = '') {  /// ********** TO DO ********
        $db = $this->open();
        if (empty($field_name)) {
            $querystring = "SELECT * FROM $object WHERE $condition";
            try {
                $sql = $db->prepare($querystring);
                $sql->execute();
                if ($sql->rowCount() >= 1) {
                    return 1;
                } else {
                    return 0;
                }
            } catch (PDOException $ex) {
                return $ex->getMessage();
            }
        } else {
            $querystring = "SELECT $field_name FROM $object WHERE $condition";
            try {
                $sql = $db->prepare($querystring);
                $sql->execute();
                if ($sql->rowCount() >= 1) {
                    return $sql->fetch(PDO::FETCH_OBJ)->$field_name;
                } else {
                    return 0;
                }
            } catch (PDOException $ex) {
                return $ex->getMessage();
            }
        }
    }

    public function ObjectPrimaryKey($object) {
        try {
            $db = $this->open();
            $querystring = "SELECT column_name
                        FROM information_schema.key_column_usage
                        WHERE table_catalog='price_matrix'  AND table_schema='public' AND table_name='" . $object . "'";
            $sql = $db->prepare($querystring);
            $sql->execute();
            if ($sql->rowCount() >= 1) {
                return $sql->fetch(PDO::FETCH_OBJ)->column_name;
            }
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    public function QueryResultDDL($querystring, $return_type = '') { // only query result
        try {
            $db = $this->open();
            $sql = $db->prepare($querystring);

            if ($sql->execute() == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    public function QueryWithQueryString($queryString, $return_type = '') {

        try {
            $db = $this->open();
            $sql = $db->prepare($queryString);
            $sql->execute();
            $count = $sql->rowCount();
            if ($count >= 1) {
                if ($return_type == "array") {
                    return $sql->fetchAll(PDO::FETCH_OBJ);
                } else if ($return_type == "json") {
                    return json_encode($sql->fetchAll(PDO::FETCH_OBJ));
                } else {
                    return $sql->fetchAll(PDO::FETCH_OBJ);
                }
            } else {
                return "Objects NOT Found";
            }
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    public function CheckTableExists($database_name, $table_name) {

        try {
            $db = $this->open();
            $querystring = "SELECT *  FROM information_schema.tables WHERE table_catalog='" . $database_name . "'  AND table_name = '" . $table_name . "'";
            $sql = $db->prepare($querystring);
            $sql->execute();
            if ($sql->rowCount() >= 1) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    public function DatabaseColumnNames($database_name, $table_name, $column_name = '') {

        if (!empty($column_name)) {
            $db = $this->open();
            $queryString = "SELECT column_name
FROM information_schema.columns
WHERE table_catalog='" . $database_name . "'  AND table_schema='public' AND table_name='" . $table_name . "' AND column_name='" . $column_name . "'";
            $sql = $db->prepare($queryString);
            $sql->execute();
            $count = $sql->rowCount();
            if ($count >= 1) {
                $columns = $sql->fetch(PDO::FETCH_OBJ);
                $column_name = $columns{0}->column_name;
                return $column_name;
            } else {
                return 0;
            }
        } else {
            $db = $this->open();
            $queryString = "SELECT column_name
FROM information_schema.columns
WHERE table_catalog='" . $database_name . "'  AND table_schema='public' AND table_name='" . $table_name . "'";

            try {
                $sql = $db->prepare($queryString);
                $sql->execute();
                if ($sql->rowCount() >= 1) {
                    return $sql->fetchAll(PDO::FETCH_OBJ);
                } else {
                    return 0;
                }
            } catch (PDOException $ex) {
                return $ex->getMessage();
            }
        }
    }

    public function QueryWithQueryStringWithFatchAssoc($queryString) {

        try {
            $db = $this->open();
            $sql = $db->prepare($queryString);
            $sql->execute();
            $count = $sql->rowCount();
            if ($count >= 1) {
                return $sql->fetchAll(PDO::FETCH_ASSOC);
            } else {
                return "Objects NOT Found";
            }
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    public function SelectAll($object, $condition = '', $object_array = '', $return_type = '') {

        $query = "";
        if (!empty($condition)) {
            $query = "SELECT * FROM $object WHERE $condition";
        } else if (!empty($object_array)) {
            $count = 0;
            $fields = '';
            if (count($object_array) <= 1) {
                foreach ($object_array as $col => $val) {
                    if ($count++ != 0)
                        $fields .=', ';
                    $fields .="\"" . $col . "\" ='$val' ";
                }

                $query = "SELECT * FROM $object WHERE $fields";
            }else {
                $query = "SELECT * FROM $object WHERE $fields";
            }
        } else {
            $query = "SELECT * FROM $object";
        }

        try {
            $db = $this->open();
            $sql = $db->prepare($query);
            $sql->execute();
            $count = $sql->rowCount();
            if ($count >= 1) {
                if ($return_type == "array") {
                    return $sql->fetchAll(PDO::FETCH_OBJ);
                } else if ($return_type == "json") {
                    return json_encode($sql->fetchAll(PDO::FETCH_OBJ));
                } else {
                    return $sql->fetchAll(PDO::FETCH_OBJ);
                }
            }
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    /* Start CRUD Command  */

    public function insert($object, $object_array, $primary_key_name) {
        $db = $this->open();
        $count = 0;
        //$ccount =0;
        $querystring = "INSERT INTO " . $object . " ";
        $fields = "(";
        $values = "(";
        foreach ($object_array as $col => $val) {
            if ($count++ != 0) {
                $fields .=', ';
                $values .=', ';
            }
            $fields .= $col;
            if (gettype($val) == "boolean") {
                $values .= $val;
            } else if (empty($val)) {
                $values .= "NULL";
            } else if (gettype($val) == "double") {
                $values .= $val;
            } else if (gettype($val) == "NULL") {
                $values .= "NULL";
            } else if (gettype($val) == "string") {
                $values .="'" . $val . "'";
            } else if (gettype($val) == "boolean") {
                $values .= $val;
            } else {
                $values .="'" . $val . "'";
            }
        }
        $fields .= ")";
        $values .=")";
        try {
            $querystring .= $fields . " VALUES " . $values;
            $sql = $db->prepare($querystring);
            if ($sql->execute()) {
                $rsql = $db->prepare("SELECT * FROM $object ORDER BY $primary_key_name DESC LIMIT 1");
                $rsql->execute();
                $res = $rsql->fetch(PDO::FETCH_OBJ);
                $last_insert_id = $res->{"$primary_key_name"};
                return $last_insert_id;
            } else {
                return 0;
            }
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    public function update($object, $object_array, $condition = '') {

        if (empty($condition)) {
            if (count($object_array) >= 2) {
                $con_key_from_arr = array_keys($object_array);
                $key = $con_key_from_arr[0];
                $value = array_shift($object_array);
                $fields = array();

                foreach ($object_array as $field => $val) {
                    if (empty($val)) {
                        $fields[] = $field . "=NULL";
                    } else {
                        $fields[] = $field . "=" . "'" . $val . "'";
                    }
                    //  $fields[] = $field . "=" . "'" . $val . "'";
                }

                $querystring = "UPDATE " . $object . " SET " . join(' ,', $fields) . " WHERE " . $key . "=" . "'" . $value . "'";
                try {
                    $db = $this->open();
                    $sql = $db->prepare($querystring);
                    $sql->execute();
                    return 1;
                } catch (PDOException $ex) {
                    return $ex->getMessage();
                }
            } else {
                return "Update Array is in wrong format";
            }
        } else {
            $fields = array();
            foreach ($object_array as $field => $val) {
                $fields[] = $field . "=" . "'" . $val . "'";
            }
            $query .= "UPDATE `$object` SET " . join(', ', $fields) . " WHERE  $condition";

            try {
                $db = $this->open();
                $sql = $db->prepare($query);
                $sql->execute();
                return 1;
            } catch (PDOException $ex) {
                return $ex->getMessage();
            }
        }
    }

    public function delete($object, $object_array, $condition = '') {
        $querystring = "DELETE FROM " . $object . " WHERE ";
        if (count($object_array) == 1) {
            foreach ($object_array as $col => $val) {
                $querystring .= $col . "=" . "'" . $val . "'";
                break;
            }
            try {
                $db = $this->open();
                $sql = $db->prepare($querystring);
                $sql->execute();
                return 1;
            } catch (PDOException $ex) {
                return $ex->getMessage();
            }
        } else {
            return 0;
        }
    }

    public function dump($object) {
        echo "<pre>";
        print_r($object);
        echo "</pre>";
    }

    public function login($username, $password) {
        $count = 0;
        $fields = '';
        try {
            $dbcon = $this->open();
            $query = "SELECT * FROM employee WHERE " . " email_address='" . $username . "' and password='" . $password . "'";
            $sql = $dbcon->prepare($query);
            $sql->execute();

            if ($sql->rowCount() >= 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (PDOException $ex) {
            return $ex->getMessage();
        }
    }

    public function one_many_relation($object_array, $fields_array, $condition = '', $return_type) {
        
    }

    /* End CRUD Command    */
}

//$con = new connection();
//$con->open();

////$con->SelectAll("sssrbo");
//$object_array = array("id" => 1, "name" => "Aushan");
//echo "<pre>";
//$con->update("rbo", $object_array);
//echo "</pre>";